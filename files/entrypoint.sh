#!/usr/bin/env bash
# Used Applications
# shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
NGINX="$(command -v nginx)"
PHP_FPM="$(command -v php-fpm${PHP_VERSION})"
# shellcheck disable=SC2086 # Double quote to prevent globbing and word splitting.
CONFIG_FILE="/etc/php/${PHP_VERSION}/fpm/php-fpm.conf"
# Execute PHP
${PHP_FPM} --nodaemonize -R -y "${CONFIG_FILE}" &
# Execute Nginx
${NGINX}  -c /etc/nginx/nginx.conf