FROM registry.gitlab.com/pazdzewicz_docker/phalcon-nginx:master

ARG PHP_VERSION="7.3"
ARG PHALCON_VERSION="4"
ARG COMPOSER_VERSION="1.10.13"

ENV PHP_VERSION ${PHP_VERSION}
ENV COMPOSER_VERSION ${COMPOSER_VERSION}
ENV PHALCON_VERSION ${PHALCON_VERSION}

COPY files/entrypoint.sh /entrypoint.sh

COPY files/phalcon.conf /etc/nginx/conf.d/phalcon.conf

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
                    curl \
                    apt-transport-https \
                    lsb-release \
                    ca-certificates \
                    lsb-release \
                    gnupg \
                    git \
                    unzip && \
    curl https://packages.sury.org/php/apt.gpg | apt-key add - && \
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" >> /etc/apt/sources.list.d/php.list && \
    apt-get update -y && \
    echo ${PHP_VERSION} && \
    apt-get install -y --no-install-recommends \
                    php${PHP_VERSION}-opcache \
                    php${PHP_VERSION}-bcmath \
                    php${PHP_VERSION}-cli \
                    php${PHP_VERSION}-common \
                    php${PHP_VERSION}-curl \
                    php${PHP_VERSION}-fpm \
                    php${PHP_VERSION}-gmp \
                    php${PHP_VERSION}-json \
                    php${PHP_VERSION}-mbstring \
                    php${PHP_VERSION}-mysql \
                    php${PHP_VERSION}-readline \
                    php${PHP_VERSION}-xml \
                    php${PHP_VERSION}-gd \
                    php${PHP_VERSION}-tidy \
                    php${PHP_VERSION}-apcu \
                    dh-php \
                    php-common \
                    php-pear \
                    ffmpeg \
                    php${PHP_VERSION}-phalcon${PHALCON_VERSION} \
                    php${PHP_VERSION}-redis \
                    php${PHP_VERSION}-soap \
                    php${PHP_VERSION}-psr \
                    pkg-php-tools \
                    php-igbinary \
                    default-mysql-client \
                    php-dev && \
    rm -r /var/lib/apt/lists/* && \
    update-alternatives --set php /usr/bin/php${PHP_VERSION} && \
    update-alternatives --set phar /usr/bin/phar${PHP_VERSION} && \
    update-alternatives --set phar.phar /usr/bin/phar.phar${PHP_VERSION} && \
    curl https://getcomposer.org/installer > /tmp/composer-setup.php && \
    php /tmp/composer-setup.php --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION} && \
    rm -rf /tmp/composer-setup.php && \
    mkdir -p /run/php/ && \
    chmod +x /entrypoint.sh

COPY files/fpm.conf /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf
COPY files/php.ini /etc/php/${PHP_VERSION}/fpm/php.ini

WORKDIR /var/www

# Add Dockerfile to the container
# It helps for debugging & maintainability + is considered good practice
COPY Dockerfile /

CMD ["/entrypoint.sh"]
